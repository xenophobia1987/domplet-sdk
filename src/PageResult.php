<?php
namespace Domplet;

use \Iterator;

/**
 * All result returned from API should instantiate this class.
 * This class implements the iterator interface so you can
 * loop the object using foreach loop.
 * Eg: foreach ($result as $record)
 *
 * @author kentlee
 */
class PageResult implements Iterator
{
    protected $position = 0;

    /**
     * The pagination object returned from API.
     *
     * @var object
     */
    public $result;

    /**
     * The client object that retrieving this result.
     *
     * @var Client
     */
    public $cilent;

    public function __construct($result, Client $client)
    {
        $this->position = 0;

        $this->result = $result;

        $this->client = $client;
    }

    public function page()
    {
        if ($this->result->current_page > 1) {
            return intval($this->result->current_page);
        } else {
            return 1;
        }
    }

    public function totalPages()
    {
        if ($this->result->last_page) {
            return intval($this->result->last_page);
        } else {
            return 0;
        }
    }

    public function hasNextPage()
    {
        return $this->lastPage() > $this->page();
    }

    public function hasPrevPage()
    {
        return $this->page() > 1;
    }

    public function hasRecord()
    {
        return count($this->data()) > 0;
    }

    public function total()
    {
        if ($this->result->total) {
            return intval($this->result->total);
        } else {
            return 0;
        }
    }

    public function lastPage()
    {
        if ($this->result->last_page) {
            return intval($this->result->last_page);
        } else {
            return 0;
        }
    }

    public function data()
    {
        if ($this->result->data && count($this->result->data)) {
            return $this->result->data;
        } else {
            return [];
        }
    }

    /**
     * This method will call the next page if available
     * and return a new instance of PageResult.
     *
     * @return \Domplet\PageResult|NULL
     */
    public function callNextPage()
    {
        if ($this->result->next_page_url) {
            $url = $this->result->next_page_url;
            $info = parse_url($url);
            $uri = array_get($info, 'path');
            $query = array_get($info, 'query');
            $params = [];

            if ($query) {
                parse_str($query, $params);
            }

            $response = $this->client->http->call($uri, $params, 'get');

            if ($response && $response->success && $response->result) {
                return new PageResult($response->result, $this->client);
            }
        }

        return null;
    }

    /**
     * This method will call the last page if available
     * and return a new instance of PageResult.
     *
     * @return \Domplet\PageResult|NULL
     */
    public function callPrevPage()
    {
        if ($this->result->prev_page_url) {
            $url = $this->result->prev_page_url;
            $info = parse_url($url);
            $uri = array_get($info, 'path');
            $query = array_get($info, 'query');
            $params = [];

            if ($query) {
                parse_str($query, $params);
            }

            $response = $this->client->http->call($uri, $params, 'get');

            if ($response && $response->success && $response->result) {
                return new PageResult($response->result, $this->client);
            }
        }

        return null;
    }

    # Iterator Implementation.

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        $data = $this->data();

        return array_get($data, $this->position, null);
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        $data = $this->data();

        return isset($data[$this->position]);
    }
}