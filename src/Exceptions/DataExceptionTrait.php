<?php
namespace Domplet\Exceptions;

/**
 * This trait allow the exception class to return the
 * json formatted data. Eg: $exception->data->error will
 * return the error message.
 *
 * @author kentlee
 *
 */
trait DataExceptionTrait
{
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->{$property};
        } else if ($property === 'data') {
            return $this->data();
        } else {
            return null;
        }
    }

    public function data($assoc = false)
    {
        $message = $this->getMessage();

        if ($message) {
            $data = json_decode($message, $assoc);

            if (null !== $data) {
                return $data;
            }
        }

        return null;
    }
}
