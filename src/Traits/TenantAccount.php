<?php
namespace Domplet\Traits;

trait TenantAccount
{
  /**
   * Return the deposit detail based on the deposit number.
   *
   * @param  string $number
   * @return array
   */
  public function getDeposit($number)
  {
      $response = $this->http->call('api/deposits/' . $number, null, 'get');

      if ($response && $response->data) {
          return $response->data;
      } else {
          return null;
      }
  }

  /**
   * Return the deposit detail by using the third party reference number.
   *
   * @param  string $reference
   * @return array
   */
  public function getDepositByReference($reference)
  {
      $response = $this->http->call('api/deposits/reference/' . $reference, null, 'get');

      if ($response && $response->data) {
          return $response->data;
      } else {
          return null;
      }
  }
}
