<?php
namespace Domplet;

use \Exception;
use \RuntimeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Domplet\Exceptions\AuthenticationException;
use Domplet\Exceptions\BadRequestException;
use Domplet\Exceptions\InputException;
use Domplet\Exceptions\ServerResponseException;

class HttpClient
{
    public $http;

    /**
     * @var string The access token to be added in the header.
     */
    public $token;

    public function __construct($apiUri, $options)
    {
        $this->http = new Client([
            'base_uri' => $apiUri,
            'timeout' => array_get($options, 'timeout', 15)
        ]);
    }

    /**
     * To make a standard API call. The first parameter
     * is the URI endpoint. Eg: /api/companies
     * The second parameter is the parameter to
     * append in the http request (Applicable for post
     * and get).
     * The third parameter indicate the HTTP verb.
     * The last parameter allow customization of the
     * http request. You can add header here.
     *
     * @return object
     * @throw
     */
    public function call($endpoint, $params = null, $method = 'get', $options = [])
    {
        $method = strtoupper($method);

        if ($params && count($params)) {
            if ($method === 'GET') {
                $options['query'] = $params;
            } else {
                $options['form_params'] = $params;
            }
        }

        $headers = [
            'Accept' => array_get($options, 'accept', 'application/json'),
        ];

        if ($this->token) {
            $headers['Authorization'] = 'Bearer ' . $this->token;
        }

        $options['headers'] = $headers;

        try {
            $response = $this->http->request($method, $endpoint, $options);

            $body = $response->getBody();

            $data = json_decode($body);

            if ($data !== null) {
                return $data;
            } else {
                throw new ServerResponseException($body);
            }
        } catch (Exception $ex) {
            if ($ex instanceof ClientException) {
                $response = $ex->getResponse();
                $statusCode = $response->getStatusCode();
                $body = $response->getBody();

                if ($statusCode === 401) {
                    throw new AuthenticationException($body, $ex->getCode());
                } else if ($statusCode === 422) {
                    // Form validation error.
                    throw new InputException($body, $ex->getCode());
                } else if ($statusCode === 400) {
                    throw new BadRequestException($body, $ex->getCode());
                } else {
                    throw $ex;
                }
            } else {
                throw $ex;
            }
        }
    }

    /**
     * To upload files and post to a specified endpoint.
     *
     * @param  string $endpoint The URL endpoint to call the API.
     * @param  array  $files    An array of files. It can be filepath or file pointer which point to the file.
     * @param  array  $params   Any other parameter to post along with this request.
     * @param  string $method   The http verb. Default is post.
     * @param  array  $options  Additional options to send to Guzzle Client.
     * @return object
     */
    public function upload($endpoint, $files, $params = [], $method = 'post', $options = [])
    {
        $postParams = [];

        if (count($files)) {
            foreach ($files as $name => $file) {
                if (is_resource($file) && (get_resource_type($file) == 'file' || get_resource_type($file) == 'stream')) {
                    $postParams[] = [
                        'name' => $name,
                        'content' => $file,
                    ];
                } else if (is_array($file)) {
                    $fp = array_get($file, 'file');
                    $filename = array_get($file, 'filename');

                    if ($fp) {
                        if (is_string($fp)) {
                            $fp = fopen($fp, 'r');
                        }

                        $postParams[] = [
                            'name' => $name,
                            'contents' => $fp,
                            'filename' => $filename,
                        ];
                    } else {
                        throw new RuntimeException('Unable to find the file pointer or filename in the $files parameter.');
                    }
                } else if (is_string($file)) {
                    $postParams[] = [
                        'name' => $name,
                        'contents' => fopen($file, 'r'),
                    ];
                } else {
                    throw new RuntimeException('Invalid file type provided to upload.');
                }
            }
        }

        if (count($params)) {
            foreach ($params as $name => $value) {
                $postParams[] = [
                    'name' => $name,
                    'contents' => $value,
                ];
            }
        }

        $options['multipart'] = $postParams;

        return $this->call($endpoint, null, $method, $options);
    }
}
