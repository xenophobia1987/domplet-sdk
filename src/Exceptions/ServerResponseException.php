<?php
namespace Domplet\Exceptions;

use \Exception;

/**
 * Throw this exception when the API server response something
 * that is not in expected format.
 *
 * @author kentlee
 */
class ServerResponseException extends Exception
{
}
