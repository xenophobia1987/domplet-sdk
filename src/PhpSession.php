<?php
namespace Domplet;

class PhpSession implements SessionInterface
{
    public function initialize()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function store($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public function get($key, $default = null)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return $default;
        }
    }

    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    public function remove($key)
    {
        unset($_SESSION[$key]);
    }
}
