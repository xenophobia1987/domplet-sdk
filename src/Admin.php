<?php
namespace Domplet;

use \Exception;
use Domplet\Traits\BasicUser;

class Admin extends Client
{
    use BasicUser;

    public function callLogin($username, $password)
    {
        $response = $this->http->call('oauth/admin/login', [
            'username' => $username,
            'password' => $password
        ], 'post');

        return $response;
    }

    /**
     * This method query and return the list of tenants created.
     *
     * @param array $params
     * @return PageResult|NULL
     */
    public function tenants($params = [])
    {
        try {
            $response = $this->http->call('api/admin/tenants', $params, 'get');

            if ($response && $response->success) {
                return new PageResult($response->result, $this);
            } else {
                return null;
            }
        } catch (Exception $ex) {
            $this->error($ex);
        }
    }

    public function tenantDetail($tenantCode)
    {
        try {
            $response = $this->http->call('api/admin/tenants/' . $tenantCode, null, 'get');
        } catch (Exception $ex) {
            $this->error($ex);
        }
    }
}
