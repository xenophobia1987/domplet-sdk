<?php
namespace Domplet;

use \RuntimeException;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use Domplet\Traits\BasicUser;

class Site extends Client
{
    use BasicUser;

    const SITE_SESSION_TOKEN_NAME = '_site_access_token';

    public function __construct($options = [])
    {
        $this->initialize($options, self::SITE_SESSION_TOKEN_NAME);

        // Check if the session did logged in.
        if (!$this->isLoggedIn(self::SITE_SESSION_TOKEN_NAME)) {
            if (!$this->ensureLogin()) {
                throw new RuntimeException('Unable to obtain the access token for site client.');
            }
        }
    }

    public function callLogin($username, $password)
    {
        // We never use this method anyway.
        return null;
    }

    /**
     * Register a new member. You must pass the correct parameter
     * to register a member. Depend on your agent site whether it's
     * using phone, email or username as authentication identifier.
     *
     * If the registration success, it will return the user profile
     * data (object).
     * Otherwise, false return.
     *
     * @param  array $params
     * @return object|boolean
     */
    public function registerMember($params)
    {
        $response = $this->http->call('api/register', $params, 'post');

        if ($response && $response->success) {
            return $response->user;
        } else {
            return false;
        }
    }

    public function verifyMember($username, $code)
    {
        $response = $this->http->call('api/verify', [
            'username' => $username,
            'code' => $code
        ], 'post');

        if ($response && $response->success) {
            return $response->user;
        } else {
            return false;
        }
    }

    public function checkUsername($username)
    {
        $response = $this->http->call('api/check-username', [
            'username' => $username,
        ], 'post');

        if ($response && !$response->used) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the bank accounts of the current tenant.
     * This will return the array of banks if found,
     * otherwise, false will return.
     *
     * @return array|null|boolean
     */
    public function bankAccounts()
    {
        $response = $this->http->call('api/tenants/banks');

        if ($response && $response->success) {
            if (isset($response->banks) && $response->banks) {
                return $response->banks;
            } else {
                return null;
            }
        } else {
            return false;
        }
    }

    /**
     * This method will ensure the site client has the credential
     * to call any API from the server. It will auto renew the
     * API token if it is expired.
     *
     * @return boolean
     */
    public function ensureLogin()
    {
        // Find the latest token file in server.
        $path = $this->storagePath;
        $pattern = $path . DIRECTORY_SEPARATOR . 'client_*.json';

        $files = glob($pattern);

        if ($files && count($files)) {
            foreach ($files as $file) {
                $matches = null;
                $pathinfo = pathinfo($file);

                if (preg_match('/\_(\d+)\.json/', $file, $matches)) {
                    $timestamp = intval($matches[1]);

                    $tokenDatetime = Carbon::createFromTimestamp($timestamp);

                    // Check if the tokenDatetime more than the expiry.
                    if ($tokenDatetime->addDays($this->tokenExpiry)->lt(Carbon::now())) {
                        // This token is expired, renew it.
                        // Before that, we need to delete this file first.
                        if (!@unlink($file)) {
                            throw new RuntimeException('Unable to remove old token file: ' . $file);
                        }

                        $response = $this->renewToken();

                        if ($response) {
                            // Store this token in a file.
                            $tokenFilename = 'client_' . time() . '.json';
                            $content = json_encode($response);

                            if (!$this->file->write($tokenFilename, $content)) {
                                throw new RuntimeException('Unable to write the access token with specified storage path: ' . $this->storagePath);
                            }

                            $this->session->store(self::SITE_SESSION_TOKEN_NAME, $tokenFilename);

                            $this->http->token = $response->access_token;

                            return true;
                        }
                    } else {
                        // This token is still valid, use it.
                        $content = file_get_contents($file);

                        $data = json_decode($content, true);

                        // Store the unique filename into session for next request.
                        $this->session->store(self::SITE_SESSION_TOKEN_NAME, $pathinfo['basename']);

                        if (isset($data['access_token'])) {
                            $this->http->token = array_get($data, 'access_token');

                            return true;
                        }
                    }
                }
            }
        } else {
            // No token file found at all, create one.
            $response = $this->renewToken();

            if ($response) {
                // Store this token in a file.
                $tokenFilename = 'client_' . time() . '.json';
                $content = json_encode($response);

                if (!$this->file->write($tokenFilename, $content)) {
                    throw new RuntimeException('Unable to write the access token with specified storage path: ' . $this->storagePath);
                }

                $this->session->store(self::SITE_SESSION_TOKEN_NAME, $tokenFilename);

                $this->http->token = $response->access_token;

                return true;
            }
        }

        return false;
    }

    public function approveDeposit($numbers)
    {
        if (!is_array($numbers)) {
            $numbers = [$numbers];
        }

        $response = $this->http->call('api/deposits/approve', [
            'numbers' => $numbers
        ], 'post');

        if ($response->success) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the deposit detail based on the deposit number.
     *
     * @param  string $number
     * @return array
     */
    public function getDeposit($number)
    {
        $response = $this->http->call('api/deposits/' . $number, null, 'get');

        if ($response && $response->data) {
            return $response->data;
        } else {
            return null;
        }
    }

    /**
     * Return the deposit detail by using the third party reference number.
     *
     * @param  string $reference
     * @return array
     */
    public function getDepositByReference($reference)
    {
        $response = $this->http->call('api/deposits/reference/' . $reference, null, 'get');

        if ($response && $response->data) {
            return $response->data;
        } else {
            return null;
        }
    }

    protected function renewToken()
    {
        $response = $this->http->call('oauth/token', [
            'grant_type' => 'client_credentials',
            'client_id' => $this->appId,
            'client_secret' => $this->secret,
            'scope' => '',
        ], 'post');

        if ($response && $response->access_token) {
            return $response;
        } else {
            return false;
        }
    }
}
