<?php
namespace Domplet\Exceptions;

use \Exception;

/**
 * Throw this exception if the server response error 400.
 *
 * @author kentlee
 *
 */
class BadRequestException extends Exception
{
    use DataExceptionTrait;
}
