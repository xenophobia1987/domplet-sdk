<?php
namespace Domplet\Exceptions;

use \Exception;

/**
 * Authentication error.
 *
 * @author kentlee
 *
 */
class AuthenticationException extends Exception
{
    use DataExceptionTrait;
}
