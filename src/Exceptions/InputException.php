<?php
namespace Domplet\Exceptions;

use \Exception;

class InputException extends Exception
{
    use DataExceptionTrait;

    /**
     * Flattern the error messages into single dimension array.
     *
     * @return array
     */
    public function allMessages()
    {
        if ($this->data && $this->data->message) {
            $messages = [];

            foreach ($this->data->message as $field => $errorMessages) {
                if (is_array($errorMessages) && count($errorMessages)) {
                    foreach ($errorMessages as $message) {
                        $messages[] = $message;
                    }
                }
            }

            return $messages;
        } else {
            return null;
        }
    }

    /**
     * This method return the messages of the exception.
     * It will return an array of messages.
     *
     * @param string $fieldName
     * @return array|null
     */
    public function getMessages($fieldName = null)
    {
        if ($fieldName) {
            if ($this->hasError($fieldName)) {
                return $this->data->message->{$fieldName};
            } else {
                return null;
            }
        } else {
            return (array) $this->data->message;
        }
    }

    public function firstMessage($fieldName = null)
    {
        if ($fieldName) {
            if ($this->data && $this->data->message) {
                $message = $this->data->message;

                if (isset($message->{$fieldName})) {
                    $messages = $message->{$fieldName};

                    if ($messages && count($messages)) {
                        return $messages[0];
                    }
                }
            }

            return null;
        } else {
            $messages = $this->allMessages();

            if ($messages) {
                return $messages[0];
            } else {
                return null;
            }
        }
    }

    public function hasError($fieldName)
    {
        if ($this->data && $this->data->message && isset($this->data->message->{$fieldName})) {
            return true;
        } else {
            return false;
        }
    }
}
