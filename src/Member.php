<?php
namespace Domplet;

use Domplet\Traits\BasicUser;

class Member extends Client
{
    use BasicUser;

    public function callLogin($username, $password)
    {
        $response = $this->http->call('oauth/login', [
            'grant_type' => 'password',
            'client_id' => $this->appId,
            'client_secret' => $this->secret,
            'username' => $username,
            'password' => $password,
            'scope' => ''
        ], 'post');

        return $response;
    }

    /**
     * A proxy method to Site::registerMember.
     *
     * @param  array $params
     * @return object|boolean
     */
    public function register($params)
    {
        return $this->siteClient()->registerMember($params);
    }

    /**
     * This method will save a deposit transaction (unverify)
     * and return the transaction number. It can use to send
     * to payment gateway as unique transaction ID.
     *
     * Once verified, you can call the system/tenant deposits/approve
     * API to verify it. Credits will added to user wallet
     * once verified.
     *
     * @param  float $amount  The amount to deposit
     * @param  string $method  The deposit method. Currently available cdm, online_banking
     * @param  array  $options Accepts an array with following keys: cashback_id, bank, description, receipt, receipt_date
     * @return string|boolean It will return the transaction number if success, false otherwise.
     */
    public function deposit($amount, $method, $options = [])
    {
        $params = [
            'amount' => $amount,
            'payment_method' => $method,
        ];

        if (count($options)) {
            $params = array_merge($params, $options);
        }

        if (isset($params['receipt']) && $params['receipt']) {
            // This require upload.

            // We need pass the other fields without receipt.
            $fields = $params;
            unset($fields['receipt']);

            $response = $this->http->upload('api/deposits/save', [
                'receipt' => $params['receipt']
            ], $fields, 'post');
        } else {
            // Use normal post.
            $response = $this->http->call('api/deposits/save', $params, 'post');
        }

        if ($response->success) {
            return $response->number;
        } else {
            return false;
        }
    }

    /**
     * Return the deposit detail based on the deposit number.
     *
     * @param  string $number
     * @return array
     */
    public function getDeposit($number)
    {
        $response = $this->http->call('api/deposits/' . $number, null, 'get');

        if ($response && $response->data) {
            return $response->data;
        } else {
            return null;
        }
    }

    /**
     * Return the deposit detail by using the third party reference number.
     *
     * @param  string $reference
     * @return array
     */
    public function getDepositByReference($reference)
    {
        $response = $this->http->call('api/deposits/reference/' . $reference, null, 'get');

        if ($response && $response->data) {
            return $response->data;
        } else {
            return null;
        }
    }

    /**
     * To list the deposits from server.
     * @param  array $options
     * @return
     */
    public function deposits($options = [])
    {
        $response = $this->http->call('api/deposits', $options, 'get');

        return $response;
    }

    public function withdraw($amount, $bank, $description = null)
    {
        $response = $this->http->call('api/withdraws/save', [
            'amount' => $amount,
            'bank' => $bank,
            'description' => $description,
        ], 'post');

        if ($response->success) {
            return $response->number;
        } else {
            return false;
        }
    }

    public function createBankAccount($data)
    {
        $response = $this->http->call('api/accounts/bank-accounts/new', $data, 'post');

        if ($response->success) {
            return true;
        } else {
            return false;
        }
    }

    public function updateBankAccount($bankCode, $data)
    {
        $response = $this->http->call('api/accounts/bank-accounts/' . $bankCode, $data, 'patch');

        if ($response->success) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBankAccount($bankCode)
    {
        $response = $this->http->call('api/accounts/bank-accounts/' . $bankCode, null, 'delete');

        if ($response->success) {
            return true;
        } else {
            return false;
        }
    }
}
