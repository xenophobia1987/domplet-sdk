<?php
require_once('vendor/autoload.php');

$client = new Domplet\Member([
    'app_id' => 3,
    'secret' => 'de5dd80a3457ef36efe94a263170f193',
    'throw_exception' => true
]);

if ($client->isLoggedIn()) {
    // echo 'Welcome ' . $client->name . '! You have RM' . $client->credits . ' left.' . PHP_EOL;
    echo 'Welcome ' . $client->name . '!' . PHP_EOL;

    $number = $client->withdraw(30, 'e1e2cb469413vh3', 'From SDK');

    if ($number) {
        echo 'Withdraw: ' . $number . PHP_EOL;
    }

    /*
    $tenants = $client->tenants();

    if ($tenants->hasRecord()) {
        foreach ($tenants as $tenant) {
            echo $tenant->name;
        }
    } else {
        echo ' Unable to get tenant.';
    }
    */
} else {
    // User not logged in yet.
    echo 'User not logged in. Try login.' . PHP_EOL;

    if ($client->login('jason.teh2', 'password')) {
        echo 'Login successful.' . PHP_EOL;
    }
}
