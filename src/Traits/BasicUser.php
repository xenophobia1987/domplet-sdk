<?php
namespace Domplet\Traits;

use \InvalidArgumentException;
use \Exception;

/**
 * This trait include the basic function of all users in the wallet
 * system.
 *
 * @author kentlee
 */
trait BasicUser
{
    public function updatePassword($newPassword)
    {
        if (!$newPassword) {
            throw new InvalidArgumentException('Cannot accept empty password to update.');
        }

        try {
            $response = $this->http->call('api/accounts/password/update', [
                'password' => $newPassword,
            ], 'post');

            return $response->success ? true : false;
        } catch (Exception $ex) {
            $this->error($ex);
        }
    }

    public function changePassword($currentPassword, $password, $passwordConfirmation)
    {
        try {
            $response = $this->http->call('api/accounts/password/change', [
                'old_password' => $currentPassword,
                'password' => $password,
                'password_confirmation' => $passwordConfirmation,
            ], 'post');

            return $response->success ? true : false;
        } catch (Exception $ex) {
            $this->error($ex);
        }
    }

    public function getCredits($update = true)
    {
        try {
            $response = $this->http->call('api/accounts/credits');

            if ($response && $response->success) {
                $credits = floatval($response->credits);

                if ($update) {
                    $this->tokenData->credits = $credits;
                    $this->updateTokenData();
                }

                return $credits;
            } else {
                return false;
            }
        } catch (Exception $ex) {
            $this->error($ex);
        }
    }

    /**
     * Return the user's profile. The second parameter tell whether to update
     * the json file stored.
     *
     * If no profile found, false will be returned.
     *
     * @param boolean $update
     * @return object|boolean
     */
    public function getProfile($update = true)
    {
        try {
            $response = $this->http->call('api/accounts', [
                'bank' => 1,
            ], 'get');

            if ($response && $response->success) {
                $data = $response->data;

                if ($update) {
                    // Update the token data.
                    $arrayData = (array) $data;

                    foreach ($arrayData as $name => $value) {
                        if (property_exists($this->tokenData, $name)) {
                            $this->tokenData->{$name} = $value;
                        }
                    }

                    $this->updateTokenData();
                }

                return $data;
            } else {
                // Try return the tokenData.
                if ($this->tokenData) {
                    return $this->tokenData;
                } else {
                    return false;
                }
            }
        } Catch (Exception $ex) {
            $this->error($ex);
        }
    }

    /**
     * Return the current logged in user data in array format.
     *
     * @param  array  $excepts
     * @return array
     */
    public function userData($excepts = [])
    {
        if ($this->tokenData) {
            $data = (array) $this->tokenData;

            if (count($excepts)) {
                foreach ($excepts as $name) {
                    if (isset($data[$name])) {
                        unset($data[$name]);
                    }
                }
            }

            return $data;
        } else {
            return null;
        }
    }

    public function getCreditsAttribute()
    {
        if ($this->tokenData && $this->tokenData->credits) {
            return floatval($this->tokenData->credits);
        } else {
            return 0;
        }
    }

    public function getNameAttribute()
    {
        if ($this->tokenData && $this->tokenData->name) {
            return $this->tokenData->name;
        } else {
            return null;
        }
    }

    public function getTypeAttribute()
    {
        if ($this->tokenData && $this->tokenData->type) {
            return strtolower(trim($this->tokenData->type));
        } else {
            return null;
        }
    }

    public function getPhoneAttribute()
    {
      if ($this->tokenData && $this->tokenData->type) {
          return trim($this->tokenData->phone);
      } else {
          return null;
      }
    }

    public function getIdentifierAttribute()
    {
        if ($this->tokenData) {
            $data = (array) $this->tokenData;
            $value = array_get($data, $this->identifierName, null);

            if (!$value) {
                // Could be admin, usually we use username.
                $value = array_get($data, 'username', null);
            }

            return $value;
        } else {
            return null;
        }
    }
}
