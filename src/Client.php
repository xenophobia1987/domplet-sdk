<?php
namespace Domplet;

use \Exception;
use \RuntimeException;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

abstract class Client
{
    /**
     * @var string
     */
    protected $appId;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $storagePath;

    /**
     * @var string
     */
    protected $sessionClass;

    /**
     * @var boolean
     */
    protected $throwException;

    /**
     * @var string
     */
    protected $apiUri;

    /**
     * @var Exception
     */
    protected $lastException;

    /**
     * @var Filesystem
     */
    protected $file;

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * The identifier name.
     * username|email|phone
     *
     * @var string
     */
    protected $identifierName = '';

    /**
     * @var object
     */
    protected $tokenData;

    /**
     * The number of days to be expired on a token.
     *
     * @var int
     */
    protected $tokenExpiry;

    protected $timeout;

    /**
     * @var HttpClient
     */
    public $http;

    const SESSION_TOKEN_NAME = '_access_token';

    public function __construct($options = [])
    {
        $this->initialize($options);
    }

    /**
     * To implement accessor using get{name}Attribute format.
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->{$property};
        } else {
            $getterName = 'get' . ucfirst($property) . 'Attribute';

            if (method_exists($this, $getterName)) {
                return $this->$getterName();
            } else {
                return null;
            }
        }
    }

    /**
     * This method will initialize the options passed in the constructor.
     * If the option did not set, a default value will be used.
     *
     * @param array $options
     * @throws RuntimeException
     */
    public function initialize($options = [], $sessionKey = self::SESSION_TOKEN_NAME)
    {
        $optionMaps = [
            // config_name => property_name or [property_name, default_value]
            'app_id' => 'appId',
            'secret' => 'secret',
            'timeout' => ['timeout', 15],
            'storage_path' => ['storagePath', dirname(__DIR__)],
            'session_class' => ['sessionClass', 'Domplet\\PhpSession'],
            'throw_exception' => ['throwException', true],
            'token_expiry' => ['tokenExpiry', 365],
            'api_uri' => ['apiUri', 'http://ewallet.local/'],
            'identifier_name' => ['identifierName', 'username'],
        ];

        foreach ($optionMaps as $name => $property) {
            $value = array_get($options, $name, null);

            $default = null;
            $field = $property;

            if (is_array($property) && count($property)) {
                $field = array_get($property, 0);
                $default = array_get($property, 1);
            }

            if ($value === null) {
                $value = $default;
            }

            if (property_exists($this, $field)) {
                $this->{$field} = $value;
            }
        }

        $this->http = new HttpClient($this->apiUri, [
            'timeout' => $this->timeout
        ]);

        $adapter = new Local($this->storagePath);
        $this->file = new Filesystem($adapter);

        if (class_exists($this->sessionClass)) {
            $session = new $this->sessionClass;

            if ($session instanceof SessionInterface) {
                $session->initialize();

                $this->session = $session;
            } else {
                throw new RuntimeException('The session class did not implements the SessionInterface.');
            }
        } else {
            throw new RuntimeException('Invalid session class exists.');
        }

        if ($this->session->has($sessionKey)) {
            $sessionFile = $this->session->get($sessionKey);

            if ($this->file->has($sessionFile)) {
                $this->tokenData = json_decode($this->file->read($sessionFile));

                if ($this->tokenData && $this->tokenData->access_token) {
                    $this->http->token = $this->tokenData->access_token;
                }
            }
        }
    }

    /**
     * To login with a user. Successful logged in user will saved into session
     * and able to call the next API without login again.
     * Note not all client require login. Eg the client credential (server to server)
     * does not require login.
     *
     * @param string $username
     * @param string $password
     * @throws RuntimeException
     * @return boolean
     */
    public function login($username, $password)
    {
        if ($this->isLoggedIn()) {
            throw new RuntimeException('This session has already logged in. Please check using isLoggedIn method.');
        }

        try {
            $response = $this->callLogin($username, $password);

            if ($response) {
                // Login successful. Store the access token and save the session.
                // Save the access token in storage path with unique filename.
                $filename = md5(str_random(50) . microtime(true)) . '.json';

                if (!$this->file->write($filename, json_encode($response))) {
                    throw new RuntimeException('Unable to write the access token with specified storage path: ' . $this->storagePath);
                }

                // Store the unique filename into session for next request.
                $this->session->store(self::SESSION_TOKEN_NAME, $filename);

                if ($response->access_token) {
                    $this->http->token = $response->access_token;
                }

                $this->tokenData = $response;

                return true;
            } else {
                // Login failed.
                return false;
            }
        } catch (Exception $ex) {
            return $this->error($ex);
        }
    }
    /**
     * This function check if the user has logged in.
     *
     * @return boolean
     */
    public function isLoggedIn($key = self::SESSION_TOKEN_NAME)
    {
        if ($this->session->has($key)) {
            $tokenFilename = $this->session->get($key);

            if ($this->file->has($tokenFilename)) {
                $token = $this->file->read($tokenFilename);

                if ($token) {
                    // Verify if this token valid.
                    return true;
                }
            }
        }

        return false;
    }

    public function logout()
    {
        if (!$this->isLoggedIn()) {
            throw new \RuntimeException('This session has no logged in user.');
        }

        $filename = $this->session->get(self::SESSION_TOKEN_NAME);

        $this->session->remove(self::SESSION_TOKEN_NAME);

        $this->tokenData = null;

        // Unlink the file.
        if ($this->file->has($filename)) {
            $this->file->delete($filename);
        }

        return true;
    }

    /**
     * This method return the token filename.
     *
     * @return string
     */
    public function getTokenFilename()
    {
        if ($this->session->has(self::SESSION_TOKEN_NAME)) {
            return $this->session->get(self::SESSION_TOKEN_NAME);
        } else {
            return null;
        }
    }

    /**
     * Set the token file to the specified one. This will update the
     * session and token data for this instance as well.
     *
     * @param string $filename
     * @return boolean
     */
    public function setTokenFile($filename)
    {
        $currentTokenFilename = $this->getTokenFilename();

        if (!$currentTokenFilename || $currentTokenFilename !== $filename) {
            // Check if this token file exists.
            if ($this->file->has($filename)) {
                // Store the unique filename into session for next request.
                $this->session->store(self::SESSION_TOKEN_NAME, $filename);

                $content = json_decode($this->file->read($filename), true);

                $accessToken = array_get($content, 'access_token');

                if ($accessToken) {
                    $this->http->token = $accessToken;
                }

                $this->tokenData = $content;

                return true;
            } else {
                // Not exists, clear the token.
                $this->logout();
            }
        }

        return false;
    }

    /**
     * This method will update the current $tokenData object
     * to the token file.
     *
     * @return boolean
     */
    public function updateTokenData()
    {
        // Save the json encode string into the file.
        if ($this->isLoggedIn() && $this->file->has($this->session->get(self::SESSION_TOKEN_NAME))) {
            return $this->file->update($this->session->get(self::SESSION_TOKEN_NAME), json_encode($this->tokenData));
        } else {
            return false;
        }
    }

    /**
     * @return Exception
     */
    public function getException()
    {
        return $this->lastException;
    }

    public function error(Exception $ex)
    {
        if ($this->throwException) {
            throw $ex;
        } else {
            $this->lastException = $ex;
            return false;
        }
    }

    /**
     * This method will initialize the Site client based on the
     * app id and secret key defined.
     *
     * @return Site
     */
    public function siteClient()
    {
        $site = new \Domplet\Site([
            'app_id' => $this->appId,
            'secret' => $this->secret,
            'api_uri' => $this->apiUri,
            'timeout' => $this->timeout,
            'storage_path' => $this->storagePath,
            'session_class' => $this->sessionClass,
            'throw_exception' => $this->throwException,
            'token_expiry' => $this->tokenExpiry,
            'identifier_name' => $this->identifierName,
        ]);

        return $site;
    }

    /**
     * This method will call the API and return the response from server
     * in array.
     * This method can throw any exception, the parent method will caught
     * it and response accordingly.
     *
     * @throw Exception
     * @param string $username
     * @param string $password
     * @return array
     */
    abstract public function callLogin($username, $password);
}
