<?php
namespace Domplet;

interface SessionInterface
{
    /**
     * This is called to initialize the session driver.
     */
    public function initialize();

    public function store($key, $value);

    public function get($key, $default = null);

    public function has($key);

    public function remove($key);
}
